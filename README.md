## Geographical heat map demo

### Creating React Application:

> \$ npm install -g create-react-app

> \$ npm install -g react-native-cli

### Clone repository:

> \$ git clone git@gitlab.com:CodingTownIndia/geographical-heat-map-demo.git

> \$ cd geographical-heat-map-demo

> \$ npm install

### Run:

> \$ npm start

### Test:

> \$ npm test

### Build:

> \$ npm build

### Code Standard:

https://github.com/airbnb/javascript/tree/master/react
